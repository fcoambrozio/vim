#!/bin/bash

cd ~/.vim/fonts || exit 1

local_font_dir=~/.local/share/fonts
local_font_config_dir=~/.config/fontconfig/fonts.conf

if [ -f $local_font_config_dir ]; then
  echo "File $local_font_config_dir exists. Aborting...";
  exit 1
fi

mkdir -p $local_font_dir $local_font_config_dir

git clone https://github.com/powerline/fonts.git \
  $local_font_dir/powerline
cp 10-powerline-symbols.conf $local_font_config_dir

fc-cache -fv

